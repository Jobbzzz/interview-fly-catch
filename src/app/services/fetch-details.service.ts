import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FetchDetailsService {

  constructor(private http: HttpClient) { }


  getDetails(): Observable<any> {
    return this.http.get(
      "https://jsonplaceholder.typicode.com/posts");
  }

  postGetDetailsByID(id: number) {
    return this.http.get(
      "https://jsonplaceholder.typicode.com/posts/" + id);
  }

  postGetComment(id: number) {
    return this.http.get(
      "https://jsonplaceholder.typicode.com/posts/" + id + "/comments");
  }
}

