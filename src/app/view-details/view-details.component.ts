import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FetchDetailsService } from '../services/fetch-details.service';

@Component({
  selector: 'app-view-details',
  templateUrl: './view-details.component.html',
  styleUrls: ['./view-details.component.scss']
})
export class ViewDetailsComponent implements OnInit {
  arrFetchDetials: any;
  page: any;
  viewArray: any;
  totalItems: any;

  constructor(private service: FetchDetailsService, private router: Router) { }

  ngOnInit(): void {
    this.getDetails();
  }

  getDetails() {
    this.service.getDetails().subscribe(response => {
      this.arrFetchDetials = response;
      this.totalItems = Math.ceil(this.arrFetchDetials.length / 6);
      this.page = 1
      this.loadNewContent(this.page);
    })
  }

  onClickSeeMore(id: any) {
    this.router.navigate(['blog/viewById/' + id])
  }


  loadNewContent(page: number) {
    this.page = page;
    this.viewArray = this.arrFetchDetials.slice(6 * page - 6, 6 * page)
  }

  loadPrevious() {
    this.page = this.page - 1;
    this.viewArray = this.arrFetchDetials.slice(6 * this.page - 6, 6 * this.page)
  }

  loadNext() {
    this.page = this.page + 1;
    this.viewArray = this.arrFetchDetials.slice(6 * this.page - 6, 6 * this.page)
  }

}
