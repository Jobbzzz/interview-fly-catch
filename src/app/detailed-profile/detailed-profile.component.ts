import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FetchDetailsService } from '../services/fetch-details.service';

@Component({
  selector: 'app-detailed-profile',
  templateUrl: './detailed-profile.component.html',
  styleUrls: ['./detailed-profile.component.scss']
})
export class DetailedProfileComponent implements OnInit {
  arrDetailsById: any = [];
  isViewComments: boolean = false;
  intCurrentID: number = 0;
  arrComments: any = [];
  closeVisible: number = 0;
  intCloseCon: number = 1;



  constructor(private activeRoute: ActivatedRoute, private service: FetchDetailsService) { }

  ngOnInit() {
    this.activeRoute.params.subscribe(response => {
      this.intCurrentID = response.id
      this.service.postGetDetailsByID(this.intCurrentID).subscribe(responseData => {
        this.arrDetailsById = responseData
      })
    })
  }

  onClickComments() {
    this.intCloseCon = this.intCloseCon + 1
    if (this.intCloseCon % 2 == 0) {
      this.closeVisible = 0
    } else {
      this.closeVisible = 1
    }
    if (this.closeVisible == 0) {
      this.service.postGetComment(this.intCurrentID).subscribe(response => {
        this.arrComments = response;
        this.isViewComments = true;
      })
    } else {
      this.isViewComments = false
    }
  }


}
