import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DetailedProfileComponent } from './detailed-profile/detailed-profile.component';
import { ViewDetailsComponent } from './view-details/view-details.component';

const routes: Routes = [
  { path: '', component: ViewDetailsComponent, pathMatch: 'full' },
  { path: 'blog/home-page', component: ViewDetailsComponent },
  { path: 'blog/viewById/:id', component: DetailedProfileComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
